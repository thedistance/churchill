const Winston = require('winston');
				require('winston-daily-rotate-file');

export class Churchill {
	private winston;
	private CONFIG;
	private logger;

	constructor(CONFIG?:any) {
		this.winston = Winston;
		this.setConfig(CONFIG).makeLogger();
	}

	public setConfig(CONFIG):this {
		if(CONFIG && CONFIG.LOGGING 
			&& typeof CONFIG.LOGGING.logsDir === 'string'
			&& typeof CONFIG.LOGGING.logLevel === 'string') this.CONFIG = CONFIG;
		else if (CONFIG) throw new Error('CONFIG must have property LOGGING.logsDir and LOGGING.logLevel');

		return this;
	}

	private getDailyFileTransport():any {
		const dailyFileTransport = new this.winston.transports.DailyRotateFile({
			filename: `./${this.CONFIG.LOGGING.logsDir}/log`,	// see: https://www.npmjs.com/package/winston-daily-rotate-file 
			datePattern: `yyyy-MM-dd.`,
			prepend: true
		});
		return dailyFileTransport;
	}

	private makeLogger():this {
		this.logger = new (this.winston.Logger) ({
			transports: [
				this.getDailyFileTransport(),
				new (this.winston.transports.Console)(),
			],
			level: this.CONFIG.LOGGING.logLevel
		});
		return this;
	}

	public getLogger():any {
		return this.logger;
	}
}

