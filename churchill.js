"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Winston = require('winston');
require('winston-daily-rotate-file');
var Churchill = (function () {
    function Churchill(CONFIG) {
        this.winston = Winston;
        this.setConfig(CONFIG).makeLogger();
    }
    Churchill.prototype.setConfig = function (CONFIG) {
        if (CONFIG && CONFIG.LOGGING
            && typeof CONFIG.LOGGING.logsDir === 'string'
            && typeof CONFIG.LOGGING.logLevel === 'string')
            this.CONFIG = CONFIG;
        else if (CONFIG)
            throw new Error('CONFIG must have property LOGGING.logsDir and LOGGING.logLevel');
        return this;
    };
    Churchill.prototype.getDailyFileTransport = function () {
        var dailyFileTransport = new this.winston.transports.DailyRotateFile({
            filename: "./" + this.CONFIG.LOGGING.logsDir + "/log",
            datePattern: "yyyy-MM-dd.",
            prepend: true
        });
        return dailyFileTransport;
    };
    Churchill.prototype.makeLogger = function () {
        this.logger = new (this.winston.Logger)({
            transports: [
                this.getDailyFileTransport(),
                new (this.winston.transports.Console)(),
            ],
            level: this.CONFIG.LOGGING.logLevel
        });
        return this;
    };
    Churchill.prototype.getLogger = function () {
        return this.logger;
    };
    return Churchill;
}());
exports.Churchill = Churchill;
